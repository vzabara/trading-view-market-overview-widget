## Trading View Market Overview Widget for phpBB ##

The widget allows to show Trading View Market Overview Widget in forum header.

![ACP](screenshot-3.png)

![ACP](screenshot-2.png)

![Topic](screenshot-1.png)
See https://www.tradingview.com/widget/market-overview/
