<?php

namespace athc\tradingviewmarketwidget\acp;

class widget_info
{
	public function module()
	{
		return array(
			'filename'  => '\athc\tradingviewmarketwidget\acp\widget_module',
			'title'     => 'ACP_TRADINGVIEW_MARKET_TITLE',
			'modes'    => array(
				'settings'  => array(
					'title' => 'ACP_TRADINGVIEW_MARKET_SETTINGS',
					'auth'  => 'ext_athc/tradingviewmarketwidget && acl_a_board',
					'cat'   => array('ACP_TRADINGVIEW_MARKET_TITLE'),
				),
			),
		);
	}
}