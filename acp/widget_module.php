<?php

namespace athc\tradingviewmarketwidget\acp;

class widget_module
{
    var $u_action;

    function main($id, $mode)
    {
        global $db, $user, $template, $phpbb_log, $request;

        //$user->add_lang('tradingviewmarketwidget');

        // Set up general vars
        $action = $request->variable('action', '');
        $action = (isset($_POST['add'])) ? 'add' : ((isset($_POST['save'])) ? 'save' : $action);

        $s_hidden_fields = '';
        $widget_info = array();

        $this->tpl_name = 'acp_tradingviewmarketwidget_body';
        $this->page_title = 'ACP_TRADINGVIEW_MARKET_TITLE';

        $form_name = 'athc_tradingviewmarketwidget_settings';
        add_form_key($form_name);

        switch ($action) {
            case 'edit':

                $widget_id = $request->variable('id', 0);

                if (!$widget_id) {
                    trigger_error($user->lang['NO_WIDGET'] . adm_back_link($this->u_action), E_USER_WARNING);
                }

                $sql = 'SELECT *
					FROM ' . MARKET_OVERVIEW_TABLE . ' 
					WHERE id = ' . $widget_id;
                $result = $db->sql_query($sql);
                $widget_info = $db->sql_fetchrow($result);
                $db->sql_freeresult($result);

                $s_hidden_fields .= '<input type="hidden" name="id" value="' . $widget_id . '" />';

            case 'add':

                $template->assign_vars(array(
                        'S_EDIT_WORD'     => true,
                        'U_ACTION'        => $this->u_action,
                        'U_BACK'          => $this->u_action,
                        'SYMBOL'          => (isset($widget_info['symbol'])) ? $widget_info['symbol'] : '',
                        'LABEL'           => (isset($widget_info['label'])) ? $widget_info['label'] : '',
                        'ACTIVE'          => (isset($widget_info['active'])) ? $widget_info['active'] : 0,
                        'S_HIDDEN_FIELDS' => $s_hidden_fields,
                    )
                );
                return;

            break;

            case 'save':

                if (!check_form_key($form_name)) {
                    trigger_error($user->lang['FORM_INVALID'] . adm_back_link($this->u_action), E_USER_WARNING);
                }

                $widget_id = $request->variable('id', 0);
                $symbol = $request->variable('symbol', '', true);
                $label = $request->variable('label', '', true);
                $active = $request->variable('active', '', true);

                if ($symbol === '' || $label === '') {
                    trigger_error($user->lang['ENTER_DATA'] . adm_back_link($this->u_action), E_USER_WARNING);
                }

                $sql_ary = array(
                    'symbol' => $symbol,
                    'label'  => $label,
                    'active'  => $active ? 1 : 0,
                );

                if ($widget_id) {
                    $db->sql_query('UPDATE ' . MARKET_OVERVIEW_TABLE . ' SET ' . $db->sql_build_array('UPDATE',
                            $sql_ary) . ' WHERE id = ' . $widget_id);
                } else {
                    $db->sql_query('INSERT INTO ' . MARKET_OVERVIEW_TABLE . ' ' . $db->sql_build_array('INSERT',
                            $sql_ary));
                }

                $log_action = ($widget_id) ? 'LOG_MARKET_OVERVIEW_WIDGET_EDIT' : 'LOG_MARKET_OVERVIEW_WIDGET_ADD';

                $phpbb_log->add('admin', $user->data['user_id'], $user->ip, $log_action, false, array($widget_id));

                $message = ($widget_id) ? $user->lang['WIDGET_UPDATED'] : $user->lang['WIDGET_ADDED'];
                trigger_error($message . adm_back_link($this->u_action));

            break;

            case 'delete':

                $widget_id = $request->variable('id', 0);

                if (!$widget_id) {
                    trigger_error($user->lang['NO_WIDGET'] . adm_back_link($this->u_action), E_USER_WARNING);
                }

                if (confirm_box(true)) {
                    $sql = 'DELETE FROM ' . MARKET_OVERVIEW_TABLE . "
						WHERE id = $widget_id";
                    $db->sql_query($sql);

                    $phpbb_log->add('admin', $user->data['user_id'], $user->ip, 'LOG_COIN_WIDGET_DELETE', false,
                        array($widget_id));

                    trigger_error($user->lang['WIDGET_REMOVED'] . adm_back_link($this->u_action));
                } else {
                    confirm_box(false, $user->lang['CONFIRM_OPERATION'], build_hidden_fields(array(
                        'i'      => $id,
                        'mode'   => $mode,
                        'id'     => $widget_id,
                        'action' => 'delete',
                    )));
                }

            break;
        }

        $template->assign_vars(array(
                'U_ACTION'        => $this->u_action,
                'S_HIDDEN_FIELDS' => $s_hidden_fields,
            )
        );


        $sql = 'SELECT * FROM ' . MARKET_OVERVIEW_TABLE;
        $result = $db->sql_query($sql);

        while ($row = $db->sql_fetchrow($result)) {
            $template->assign_block_vars('widgets', array(
                    'SYMBOL'   => $row['symbol'],
                    'LABEL'    => $row['label'],
                    'ACTIVE'   => $row['active'] == 1 ? true : false,
                    'U_EDIT'   => $this->u_action . '&amp;action=edit&amp;id=' . $row['id'],
                    'U_DELETE' => $this->u_action . '&amp;action=delete&amp;id=' . $row['id'],
                )
            );
        }
        $db->sql_freeresult($result);
    }
}