<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @copyright (c) phpBB Limited <https://www.phpbb.com>
 * @license       GNU General Public License, version 2 (GPL-2.0)
 *
 * For full copyright and license information, please see
 * the docs/CREDITS.txt file.
 *
 */

namespace athc\tradingviewmarketwidget\event;

/**
 * @ignore
 */
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener
 */
class widget_listener implements EventSubscriberInterface
{
    static public function getSubscribedEvents()
    {
        return array(
            'core.user_setup'  => array(array('load_language_on_setup'), array('define_constants')),
            'core.page_header' => 'check_widget',
        );
    }

    /* @var \phpbb\controller\helper */
    protected $helper;

    /* @var \phpbb\template\template */
    protected $template;

    /* @var \phpbb\db\driver\driver_interface */
    protected $db;

    /**
     * Constructor
     *
     * @param \phpbb\controller\helper $helper   Controller helper object
     * @param \phpbb\template\template $template Template object
     */
    public function __construct(
        \phpbb\controller\helper $helper,
        \phpbb\template\template $template,
        \phpbb\db\driver\driver_interface $db
    ) {
        $this->helper = $helper;
        $this->template = $template;
        $this->db = $db;
    }

    public function define_constants()
    {
        include_once __DIR__ . '/../includes/constants.php';
    }

    /**
     * Load the Acme Demo language file
     *     acme/demo/language/en/demo.php
     *
     * @param \phpbb\event\data $event The event object
     */
    public function load_language_on_setup($event)
    {
        $lang_set_ext = $event['lang_set_ext'];
        $lang_set_ext[] = array(
            'ext_name' => 'athc/tradingviewmarketwidget',
            'lang_set' => 'tradingviewmarketwidget',
        );
        $event['lang_set_ext'] = $lang_set_ext;
    }

    /**
     * Check if we are not in the topic
     */
    public function check_widget()
    {
        global $request;

        $parts = pathinfo($request->server('SCRIPT_NAME'));
        if (in_array($parts['basename'], $this->allowedPages())) {
            $sql = 'SELECT `symbol`, `label`
            FROM ' . MARKET_OVERVIEW_TABLE . '
            WHERE active=1';
            $result = $this->db->sql_query($sql);
            $symbols = array();
            while ($row = $this->db->sql_fetchrow($result)) {
                $symbols[] = [
                    's' => $row['symbol'],
                    'd' => $row['label'],
                ];
            }
            $this->db->sql_freeresult($result);
            if (count($symbols)) {
                $height1 = 235 + count($symbols) * 55;
                $height2 = $height1;
                $this->template->assign_var('HEIGHT1', $height1);
                $this->template->assign_var('HEIGHT2', $height2);
                $this->template->assign_var('SYMBOLS', json_encode($symbols));
                $this->template->assign_var('MARKET_WIDGET', true);
            } else {
                $this->template->assign_var('EMPTY_MARKET_WIDGET', true);
            }
        }
    }

    private function allowedPages()
    {
        return array(
            'index.php',
            'posting.php',
            'viewforum.php',
            'memberlist.php',
            'app.php',
        );
    }
}
