<?php

if (!defined('IN_PHPBB')) {
    exit;
}

if (empty($lang) || !is_array($lang)) {
    $lang = array();
}

$lang = array_merge($lang, array(
    'TRADINGVIEW_MARKET_PAGE'              => 'Market Overview Widget',
    'ACP_TRADINGVIEW_MARKET_TITLE'         => 'Market Overview Widget',
    'ACP_TRADINGVIEW_MARKET_EXPLAIN'       => 'For available Symbols visit <a href="https://www.tradingview.com/widget/market-overview/" target="_blank">Market Overview Widget Settings</a>',
    'ACP_TRADINGVIEW_MARKET_WIDGETS'       => 'Market Overview Widgets',
    'ACP_TRADINGVIEW_MARKET_SETTINGS'      => 'Market Overview Widget',
    'ACP_TRADINGVIEW_MARKET_SAVED'         => 'Record have been saved successfully!',
    'EDIT_WIDGET'                          => 'Edit Widget',
    'ADD_WIDGET'                           => 'Add Widget',
    'NO_WIDGET'                            => 'Market Overview widget was not found',
    'ACP_NO_WIDGETS'                       => 'Market Overview widget was not found',
    'FORM_INVALID'                         => 'Something wrong with form data',
    'ENTER_DATA'                           => 'Data was not set',
    'WIDGET_UPDATED'                       => 'Market Overview widget was updated',
    'WIDGET_ADDED'                         => 'Market Overview widget was added',
    'WIDGET_REMOVED'                       => 'Market Overview widget was removed',
    'SYMBOL_TITLE'                         => 'Symbol',
    'LABEL_TITLE'                          => 'Label',
    'ACTIVE_TITLE'                         => 'Active',
    'MARKET_WIDGET'                        => 'Market widget',
));